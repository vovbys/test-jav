package com.ivbys.testjav.trottling;

import com.ivbys.testjav.configuration.RemoteAddrInterceptor;
import com.ivbys.testjav.exceptions.GenericBadGatewayException;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;

import static java.time.Duration.parse;
import static java.time.LocalDateTime.now;


@Component
public class ThrottlingManager {
    private final ConcurrentHashMap<IpAddrHolder, ConcurrentLinkedDeque<LocalDateTime>> storage = new ConcurrentHashMap<>();

    void handle(Method method) {
        Throttling annotation = method.getAnnotation(Throttling.class);
        String ipAddr = RemoteAddrInterceptor.getRemoteAddr().get();
        IpAddrHolder ipAddrHolder = new IpAddrHolder(ipAddr, method);

        storage.putIfAbsent(ipAddrHolder, new ConcurrentLinkedDeque<>());
        ConcurrentLinkedDeque<LocalDateTime> localDateTimes = storage.get(ipAddrHolder);

        // Clean
        LocalDateTime filterTime = now().minus(parse(annotation.duration()));
        localDateTimes.removeIf(p -> p.compareTo(filterTime) < 0);

        if (localDateTimes.size() < annotation.maxCount()) {
            localDateTimes.addFirst(now());
        } else {
            throw new GenericBadGatewayException("Too much connects from " + ipAddr);
        }
    }

    @Data
    @AllArgsConstructor
    private static class IpAddrHolder {
        String ipAddr;
        Method method;
    }
}
