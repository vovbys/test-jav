package com.ivbys.testjav.trottling;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Component
public class ThrottlingAspect {
    private final ThrottlingManager manager;

    public ThrottlingAspect(ThrottlingManager manager) {
        this.manager = manager;
    }

    @Before("@annotation(Throttling)")
    public void logExecutionTime(JoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        manager.handle(method);
    }
}
