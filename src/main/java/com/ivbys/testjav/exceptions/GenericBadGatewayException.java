package com.ivbys.testjav.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class GenericBadGatewayException extends RuntimeException {
    public GenericBadGatewayException(String message) {
        super(message);
    }
}
