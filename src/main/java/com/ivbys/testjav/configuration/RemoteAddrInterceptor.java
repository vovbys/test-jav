package com.ivbys.testjav.configuration;

import lombok.Getter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RemoteAddrInterceptor extends HandlerInterceptorAdapter {
    @Getter
    private static ThreadLocal<String> remoteAddr = new ThreadLocal<>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        remoteAddr.set(request.getRemoteAddr());
        return super.preHandle(request, response, handler);
    }
}
