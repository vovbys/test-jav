package com.ivbys.testjav;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestJavApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestJavApplication.class, args);
    }

}
