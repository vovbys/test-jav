package com.ivbys.testjav.controllers;

import com.ivbys.testjav.trottling.Throttling;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.apache.logging.log4j.util.Strings.EMPTY;


@RestController
@RequestMapping("/test/")
public class TaskController {
    @GetMapping("/get")
    @Throttling(duration = "PT1M", maxCount = 50)
    public String getEmptyString() {
        return EMPTY;
    }
}
