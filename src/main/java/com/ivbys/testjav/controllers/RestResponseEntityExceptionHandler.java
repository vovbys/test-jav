package com.ivbys.testjav.controllers;

import com.ivbys.testjav.exceptions.GenericBadGatewayException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.BAD_GATEWAY;

@RestControllerAdvice
class RestResponseEntityExceptionHandler {
    @ExceptionHandler(value = GenericBadGatewayException.class)
    @ResponseStatus(BAD_GATEWAY)
    String badGateway(Exception ex) {
        return ex.getMessage();
    }
}

